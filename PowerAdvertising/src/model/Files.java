package model;

import java.io.IOException;

import java.sql.Blob;

import java.sql.Timestamp;
import org.codehaus.jackson.map.ObjectMapper;

public class Files {
	
	private int id;
	private String name;
	private double size_mb;
	private String status;
	private Blob file;
	private Timestamp updated;
	private int user_added;
	private int user_updated;
	private Timestamp added; 
	private String file_type;

	public Files() {
	}

	public Files(int id, String name, double size_mb, String status, Blob file, Timestamp updated, int user_added, int user_updated, Timestamp added, String file_type) {
			//byte[] file, String updated, int user_added, int user_updated, String added, String file_type) {
		this.id = id;
		this.name = name;
		this.size_mb = size_mb;
		this.status = status;
		this.file = file;
		this.updated = updated;
		this.user_added = user_added;
		this.user_updated = user_updated;
		this.added = added;
		this.file_type = file_type;
	}
	
	/*public Files(int id, String name, double size_mb, String status, String updated, int user_added, int user_updated, String added) {
		this.id = id;
		this.name = name;
		this.size_mb = size_mb;
		this.status = status;
		//this.file = file;
		this.updated = updated;
		this.user_added = user_added;
		this.user_updated = user_updated;
		this.added = added;
	}*/

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public double getSize_mb() {
		return this.size_mb;
	}

	public void setSize_mb(double size_mb) {
		this.size_mb = size_mb;
	}
	
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Blob getFile() {
		return this.file;
	}

	public void setFile(Blob file) {
		this.file = file;
	}
	
	public Timestamp getUpdated() {
		return this.updated;
	}

	public void setUpdated(Timestamp timestamp) {
		this.updated = timestamp;
	}
	
	public int getUser_added() {
		return this.user_added;
	}

	public void setUser_added(int user_added) {
		this.user_added = user_added;
	}
	
	public int getUser_updated() {
		return this.user_updated;
	}

	public void setUser_updated(int user_updated) {
		this.user_updated = user_updated;
	}
	
	public Timestamp getAdded() {
		return this.added;
	}

	public void setAdded(Timestamp added) {
		this.added = added;
	}
	
	public String getFile_type() {
		return this.file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}
	
	public String toString() {
		String jsonString = "";
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonString;
	}
}
