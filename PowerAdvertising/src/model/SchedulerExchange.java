package model;

import java.sql.Timestamp;

public class SchedulerExchange {
	
	private Long id;
	private Long locationId;
	private Long deviceId;
	private Timestamp prepared;
	private Timestamp taken;
	private Timestamp lastLogfileReceived;
	private Timestamp updated;

	public SchedulerExchange() {
	}

	public SchedulerExchange(Long id, Long locationId, Long deviceId, Timestamp prepared, Timestamp taken, Timestamp updated) {
		this.id = id;
		this.locationId = locationId;
		this.deviceId = deviceId;
		this.prepared = prepared;
		this.taken = taken;
		this.updated = updated;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Timestamp getPrepared() {
		return prepared;
	}

	public void setPrepared(Timestamp prepared) {
		this.prepared = prepared;
	}

	public Timestamp getTaken() {
		return taken;
	}

	public void setTaken(Timestamp taken) {
		this.taken = taken;
	}

	public Timestamp getLastLogfileReceived() {
		return lastLogfileReceived;
	}

	public void setLastLogfileReceived(Timestamp lastLogfileReceived) {
		this.lastLogfileReceived = lastLogfileReceived;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
}