package model;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Date;
import java.sql.Timestamp;

import org.codehaus.jackson.map.ObjectMapper;

public class FilesOnDevice {

	private int id;
	private String name;
	private double size_mb;
	private String status;
	private Date updated;
	private Date added; 
	private String file_type;
	
	public FilesOnDevice() {
	}
	
	public FilesOnDevice(int id, String name, double size_mb, String status, Date updated, Date added, String file_type) {
		
		this.id = id;
		this.name = name;
		this.size_mb = size_mb;
		this.status = status;
		this.updated = updated;
		this.added = added;
		this.file_type = file_type;
		
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public double getSize_mb() {
		return this.size_mb;
	}

	public void setSize_mb(double size_mb) {
		this.size_mb = size_mb;
	}
	
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date timestamp) {
		this.updated = timestamp;
	}
	
	public Date getAdded() {
		return this.added;
	}

	public void setAdded(Date added) {
		this.added = added;
	}
	
	public String getFile_type() {
		return this.file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}
	
	public String toString() {
		String jsonString = "";
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonString;
	}
}
