package model;

import java.sql.Timestamp;
import java.util.Date;

public class DeviceLog {

	private Long id;
	private String macAddress;
	private String fileName;
	//private Date playStart;
	//private Date playEnd;
	private Timestamp playStart;
	private Timestamp playEnd;
	private String screenSize;
	private Double battery;
	private Integer connectionNumber;
	private String charging;
	private Double latitude;
	private Double longitude;
	//private Date created;
	//private Date updated;
	private Timestamp created;
	private Timestamp updated;
	private String description;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Timestamp getPlayStart() {
		return playStart;
	}
	public void setPlayStart(Timestamp playStart) {
		this.playStart = playStart;
	}
	public Timestamp getPlayEnd() {
		return playEnd;
	}
	public void setPlayEnd(Timestamp playEnd) {
		this.playEnd = playEnd;
	}
	public String getScreenSize() {
		return screenSize;
	}
	public void setScreenSize(String screenSize) {
		this.screenSize = screenSize;
	}
	public Double getBattery() {
		return battery;
	}
	public void setBattery(Double battery) {
		this.battery = battery;
	}
	public Integer getConnectionNumber() {
		return connectionNumber;
	}
	public void setConnectionNumber(Integer connectionNumber) {
		this.connectionNumber = connectionNumber;
	}
	public String getCharging() {
		return charging;
	}
	public void setCharging(String charging) {
		this.charging = charging;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Timestamp getUpdated() {
		return updated;
	}
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
