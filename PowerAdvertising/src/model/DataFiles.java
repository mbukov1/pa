package model;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

public class DataFiles {

	private String name;
	private byte[] file;
	private String file_type;
	
	public DataFiles() {
	}

	public DataFiles(String name, byte[] file, String file_type) {
			//byte[] file, String updated, int user_added, int user_updated, String added, String file_type) {
		this.name = name;
		this.file = file;
		this.file_type = file_type;
	}
	
	public byte[] getFile() {
		return this.file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getFile_type() {
		return this.file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}
	
	public String toString() {
		String jsonString = "";
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonString;
	}
}
