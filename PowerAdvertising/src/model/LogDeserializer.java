package model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.codehaus.jackson.map.JsonDeserializer;

//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.core.ObjectCodec;
////import com.fasterxml.jackson.databind.JsonDeserializer;
////import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.*;


public class LogDeserializer /*extends JsonDeserializer<String>*/ {
	
//	@Override
//	public void deserialize(JsonParser jp, DeserializationContext ctx)
//			throws IOException, JsonProcessingException {
//		try {
//			ObjectCodec oc = jp.getCodec();
//			JsonNode node = oc.readTree(jp);
//			
//			final String exportType = "OMVCASH";
//			
//			String id = node.get("transaction").get("id").asText();
//			 
//			 
//			 String idLocation = (node.get("transaction").get("id_location").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("id_location").asText());
//			 String idDevice = (node.get("transaction").get("id_device").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("id_device").asText());
//			 String login = node.get("transaction").get("id_login").asText();
//			 String username = (node.get("transaction").get("username").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("username").asText());
//			 int operatorTaxNumber = node.get("transaction").get("operator_tax_number").asInt();
//			 String customerTaxNumber = (node.get("transaction").get("customer_tax_number").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("customer_tax_number").asText());
//			 Date timestamp = DateHelper.parseISODate(node.get("transaction").get("timestamp").asText());
//			 int taxNumber = node.get("transaction").get("tax_number").asInt();
//			 String fursMessage = (node.get("transaction").get("furs_message").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("furs_message").asText());
//			 int fursStatus = node.get("transaction").get("furs_status").asInt();
//			 String eor = (node.get("transaction").get("eor").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("eor").asText());
//			 String zoi = (node.get("transaction").get("zoi").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("zoi").asText());
//			 String devClientCert_cn = (node.get("transaction").get("dev_client_cert_cn").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("dev_client_cert_cn").asText());
//			 String hsmSignature = (node.get("transaction").get("hsm_signature").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("hsm_signature").asText());
//			 String hsmClientCertPublic = (node.get("transaction").get("hsm_client_cert_public").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("hsm_client_cert_public").asText());
//			 int fursTransactionNumber = node.get("transaction").get("furs_transaction_number").asInt();
//			 String formattedNumber = (node.get("transaction").get("formatted_number").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("formatted_number").asText());
//			 String voidTransaction = (node.get("transaction").get("void_transaction").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("void_transaction").asText());
//			 int copyNumber = node.get("transaction").get("copy_number").asInt();
//			 String description = (node.get("transaction").get("description").asText().equalsIgnoreCase("null") ? null : node.get("transaction").get("description").asText());
//			 int flag = node.get("transaction").get("flag").asInt();
//			 int customerId = node.get("transaction").get("customer_id").asInt();
//			 int customerAccountId = node.get("transaction").get("customer_account_id").asInt();
//			 
//										
//			CashTransaction cTrans = new CashTransaction();
//			cTrans.setId(id);			
//			cTrans.setIdLocation(idLocation);
//			cTrans.setIdDevice(idDevice);
//			cTrans.setLogin(login);
//			cTrans.setUsername(username);
//			cTrans.setOperatorTaxNumber(operatorTaxNumber);
//			cTrans.setCustomerTaxNumber(customerTaxNumber);
//			cTrans.setTimestamp(timestamp);
//			cTrans.setTaxNumber(taxNumber);
//			cTrans.setFursMessage(fursMessage);
//			cTrans.setFursStatus(fursStatus);
//			cTrans.setEor(eor);
//			cTrans.setZoi(zoi);
//			cTrans.setDevClientCertCn(devClientCert_cn);
//			cTrans.setHsmSignature(hsmSignature);
//			cTrans.setHsmClientCertPublic(hsmClientCertPublic);
//			cTrans.setFursTransactionNumber(fursTransactionNumber);
//			cTrans.setFormattedNumber(formattedNumber);
//			cTrans.setVoidTransaction(voidTransaction);
//			cTrans.setCopyNumber(copyNumber);
//			cTrans.setDescription(description);
//			cTrans.setFlag(flag);
//			cTrans.setCustomerId(customerId);
//			cTrans.setCustomerAccountId(customerAccountId);
//			cTrans.setCreated(new Date());
//			cTrans.setCreatedBy("rmq");
//			cTrans.setExportType(exportType);
//						
//			cTrans.setTransactionItems(new ArrayList<CashTransactionItem>());
//			cTrans.setTransactionPayments(new ArrayList<CashTransactionPayment>());
//	
//			for(JsonNode i : node.get("transaction").get("transaction_item")){
//				 int idItem = i.get("id_item").asInt();
//				 //String idTransaction = i.get("id_transaction").asText(); 
//				 String idProduct = (i.get("id_product").asText().equalsIgnoreCase("null") ? null : i.get("id_product").asText());
//				 int idVehicleCategory = i.get("id_vehicle_category").asInt();
//				 int idTax = i.get("id_tax").asInt();
//				 double taxPercent = i.get("tax_percent").asDouble();
//				 int idPricelist = i.get("id_pricelist").asInt();
//				 int productType = i.get("product_type").asInt();
//				 double price = i.get("price").asDouble();
//				 double quantity = i.get("quantity").asDouble();
//				 double discount = i.get("discount").asDouble();
//				 double netAmount = i.get("net_amount").asDouble();
//				 double vatAmount = i.get("vat_amount").asDouble();
//				 double grossAmount = i.get("gross_amount").asDouble();
//				 String sellerTaxNumber = (i.get("seller_tax_number").asText().equalsIgnoreCase("null") ? null : i.get("seller_tax_number").asText());
//				 String itemDescription = (i.get("description").asText().equalsIgnoreCase("null") ? null : i.get("description").asText());
//				 String comment = (i.get("comment").asText().equalsIgnoreCase("null") ? null : i.get("comment").asText());
//				 String authorizationData = (i.get("authorization_data").asText().equalsIgnoreCase("null") ? null : i.get("authorization_data").asText());
//				 String voidItem = (i.get("void_item").asText().equalsIgnoreCase("null") ? null : i.get("void_item").asText());
//				 int itemFlag = i.get("flag").asInt();
//				 int subscriptionId = i.get("subscription_id").asInt();
//				 String materialId = (i.get("material_id").asText().equalsIgnoreCase("null") ? null : i.get("material_id").asText());				 
//				 String obuPan = i.get("obu_pan").asText();
//				 
//				 String bitType = (i.get("bit_type").asText().equalsIgnoreCase("null") ? null : i.get("bit_type").asText());
//				
//				CashTransactionItem cItem = new CashTransactionItem();
//				cItem.setIdItem(idItem);
//				cItem.setTransaction(cTrans);
//				cItem.setIdProduct(idProduct);
//				cItem.setIdVehicleCategory(idVehicleCategory);
//				cItem.setIdTax(idTax);
//				cItem.setTaxPercent(taxPercent);
//				cItem.setIdPricelist(idPricelist);
//				cItem.setProductType(productType);
//				cItem.setPrice(price);
//				cItem.setQuantity(quantity);
//				cItem.setDiscount(discount);
//				cItem.setNetAmount(netAmount);
//				cItem.setVatAmount(vatAmount);
//				cItem.setGrossAmount(grossAmount);
//				cItem.setSellerTaxNumber(sellerTaxNumber);
//				cItem.setDescription(itemDescription);
//				cItem.setComment(comment);
//				cItem.setAuthorizationData(authorizationData);
//				cItem.setVoidItem(voidItem);
//				cItem.setFlag(itemFlag);
//				cItem.setBitType(bitType);
//				cItem.setSubscriptionId(subscriptionId);
//				cItem.setMaterialId(materialId);
//				cItem.setObuPan(obuPan);
//
//				cTrans.getTransactionItems().add(cItem);
//			}
//
//			for(JsonNode i : node.get("transaction").get("transaction_payment")){
//				 int idPayment = i.get("id_payment").asInt();
//				 //String idTransaction = i.get("id_transaction").asText();
//				 int idPaymentMethodType = i.get("id_payment_method_type").asInt();
//				 int idPaymentMethod = i.get("id_payment_method").asInt();
//				 
//				 if (idPaymentMethod != 30101) {
//					 cTrans = null;
//					 return null;
//				 }
//				 
//				 double amount = i.get("amount").asDouble();
//				 int idCurrency = i.get("id_currency").asInt();
//				 double amountCurrency = i.get("amount_currency").asDouble();
//				 String paymentDescription = (i.get("description").asText().equalsIgnoreCase("null") ? null : i.get("description").asText());
//				 String paymentComment = (i.get("comment").asText().equalsIgnoreCase("null") ? null : i.get("comment").asText());
//				 String authorizationData = (i.get("authorization_data").asText().equalsIgnoreCase("null") ? null : i.get("authorization_data").asText());
//				 String voidPayment = (i.get("void_payment").asText().equalsIgnoreCase("null") ? null : i.get("void_payment").asText());
//				 int paymentFlag = i.get("flag").asInt();
//				 String cardNumber = (i.get("card_number").asText().equalsIgnoreCase("null") ? null : i.get("card_number").asText());
//				 String cardData = (i.get("card_data").asText().equalsIgnoreCase("null") ? null : i.get("card_data").asText());
//
//				CashTransactionPayment cPayment = new CashTransactionPayment();
//				cPayment.setIdPayment(idPayment);
//				cPayment.setTransaction(cTrans);
//				cPayment.setIdPaymentMethodType(idPaymentMethodType);
//				cPayment.setIdPaymentMethod(idPaymentMethod);
//				cPayment.setAmount(amount);
//				cPayment.setIdCurrency(idCurrency);
//				cPayment.setAmountCurrency(amountCurrency);
//				cPayment.setDescription(paymentDescription);
//				cPayment.setComment(paymentComment);
//				cPayment.setAuthorizationData(authorizationData);
//				cPayment.setVoidPayment(voidPayment);
//				cPayment.setFlag(paymentFlag);
//				cPayment.setCardNumber(cardNumber);
//				cPayment.setCardData(cardData);
//				
//				cTrans.getTransactionPayments().add(cPayment);
//
//			}
//			
//			return cTrans;
//		} catch (Exception e) {
//			log.error("transaction deserialization error=[{}]", e.getMessage());
//			throw new RMQDeserializationException(e);
//		}
//	}
//
//	public TRMDataManager getDataManager() {
//		return dataManager;
//	}
//
//	public void setDataManager(TRMDataManager dataManager) {
//		this.dataManager = dataManager;
//	}
//	
	
}
