package model;

import java.io.IOException;
import java.sql.Date;

import org.codehaus.jackson.map.ObjectMapper;

public class SchedulerFiles implements java.io.Serializable {

	private String day;
    private int file_id;
    private int play_order;
    private String from_time;
    private String to_time;
    private String name;
    private double size_mb;
    private String file_type;
    private String size;
    private int duration;

    public SchedulerFiles() {
    }

    public SchedulerFiles(String day, int file_id, int play_order, String from_time, String to_time, String name, double size_mb, String file_type, String size, int duration) {
        this.day = day;
        this.file_id = file_id;
        this.play_order = play_order;
        this.from_time = from_time;
        this.to_time = to_time;
        this.name = name;
        this.size_mb = size_mb;
        this.file_type = file_type;
        this.size = size;
        this.duration = duration;
    }

    public String getDay() {
        return this.day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getFile_id() {
        return this.file_id;
    }

    public void setFile_id(int file_id) {
        this.file_id = file_id;
    }

    public int getPlay_order() {
        return this.play_order;
    }

    public void setPlay_order(int play_order) {
        this.play_order = play_order;
    }

    public String getFrom_time() {
        return this.from_time;
    }

    public void setFrom_time(String from_time) {
        this.from_time = from_time;
    }

    public String getTo_time() { return this.to_time; }

    public void setTo_time(String to_time) {
        this.to_time = to_time;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSize_mb() {
        return this.size_mb;
    }

    public void setSize_mb(double size_mb) {
        this.size_mb = size_mb;
    }

    public String getFile_type() {
        return this.file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }
    
    public String getSize() {
        return this.size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getDuration() {
        return this.duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    
    public String toString() {
		String jsonString = "";
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonString;
	}
}
