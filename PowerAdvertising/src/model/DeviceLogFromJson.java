package model;

import java.sql.Timestamp;
import java.util.Date;

public class DeviceLogFromJson {
	
	private Long id;
	private String MAC_address;
	private String file_name;
	private String play_start;
	private String play_end;
	private String screen_size;
	private String battery;
	private String connection_number;
	private String charging;
	private String latitude;
	private String longitude;
	private String created;
	private String updated;
	private String description;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMacAddress() {
		return MAC_address;
	}
	public void setMacAddress(String MAC_address) {
		this.MAC_address = MAC_address;
	}
	public String getFileName() {
		return file_name;
	}
	public void setFileName(String file_name) {
		this.file_name = file_name;
	}
	public String getPlayStart() {
		return play_start;
	}
	public void setPlayStart(String play_start) {
		this.play_start = play_start;
	}
	public String getPlayEnd() {
		return play_end;
	}
	public void setPlayEnd(String play_end) {
		this.play_end = play_end;
	}
	public String getScreenSize() {
		return screen_size;
	}
	public void setScreenSize(String screen_size) {
		this.screen_size = screen_size;
	}
	public String getBattery() {
		return battery;
	}
	public void setBattery(String battery) {
		this.battery = battery;
	}
	public String getConnectionNumber() {
		return connection_number;
	}
	public void setConnectionNumber(String connection_number) {
		this.connection_number = connection_number;
	}
	public String getCharging() {
		return charging;
	}
	public void setCharging(String charging) {
		this.charging = charging;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
