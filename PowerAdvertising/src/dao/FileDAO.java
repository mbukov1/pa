package dao;

import java.sql.SQLException;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import model.Files;
import util.QueryUtil;

public class FileDAO extends QueryUtil {
	
	public static PreparedStatement preparedStatement = null;
	public static ResultSet resultSet = null;
	
	public int insertFile(Files files) throws IOException {
		int returnValue = -1;
		this.localConn = QueryUtil.getLocalConnection();
		
		/*String sql = "INSERT INTO pa.files (name, size_mb, status, file, updated, user_added, user_updated, added) " + 
					 "VALUES (?,?,?,?,?,?,?,?,?,?,?) ";*/
		
		String sql = "INSERT INTO pa.files (name, size_mb, status, user_added, user_updated) " + 
				 "VALUES (?,?,?,?,?) ";
		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			
			this.preparedStatement.setString(1, files.getName());
			this.preparedStatement.setDouble(2, files.getSize_mb());
			this.preparedStatement.setString(3, files.getStatus());
			//this.preparedStatement.setBlob(4, files.getFile());
			//this.preparedStatement.setTimestamp(5, files.getUpdated());
			this.preparedStatement.setInt(4, files.getUser_added());
			this.preparedStatement.setInt(5, files.getUser_updated());
			//this.preparedStatement.setTimestamp(8, files.getAdded());
			this.preparedStatement.executeUpdate();
			
	    	ResultSet keys = this.preparedStatement.getGeneratedKeys();
    	    if (keys.next()) {
    	    	returnValue = keys.getInt(1); //id returned after insert execution
    	        //System.out.println("returnValue = " + returnValue);
    	    } 
		} catch (Exception e) {
		  System.err.println("Got an exception insert file: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
  	  	return returnValue;
	}
  
	public void updateFile(Files files) throws IOException {
		this.localConn = QueryUtil.getLocalConnection();
		
		/*String sql = "UPDATE pa.files SET " +
				"name = ?, size_mb = ?, status = ?, file = ?, updated = ?, user_added = ?, user_updated = ?, added = ?, status = ? " + 
				"WHERE id = ? ";*/
		
		String sql = "UPDATE pa.files SET " +
				"name = ?, size_mb = ?, status = ?, user_added = ?, user_updated = ? " + 
				"WHERE id = ? ";

		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			
			this.preparedStatement.setString(1, files.getName());
			this.preparedStatement.setDouble(2, files.getSize_mb());
			this.preparedStatement.setString(3, files.getStatus());
			//this.preparedStatement.setBlob(4, files.getFile());
			///this.preparedStatement.setTimestamp(5, files.getUpdated());
			this.preparedStatement.setInt(4, files.getUser_added());
			this.preparedStatement.setInt(5, files.getUser_updated());
			this.preparedStatement.setInt(6, files.getId());
			//this.preparedStatement.setTimestamp(8, files.getAdded());
			this.preparedStatement.executeUpdate();
		} catch (Exception e) {
		  System.err.println("Update file got an exception: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Update file: Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
  	}
  
  	public void deleteFile(int fileId) throws IOException {
		this.localConn = QueryUtil.getLocalConnection();
		String sql = "DELETE FROM pa.files " +
					 "WHERE id = ? ";
		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			this.preparedStatement.setInt	(1, fileId);
			this.preparedStatement.executeUpdate();
		} catch (Exception e) {
		  System.err.println("Delete file got an exception: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Delete file got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
  	}
  	
  	public Files getFileById(int id) throws IOException {
  		Files files = new Files();
  		
		this.localConn = QueryUtil.getLocalConnection();
		String sql = "SELECT id, name, size_mb, status, file, updated, user_added, user_updated, added "
				+ "FROM pa.files WHERE id = ?";
		
		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			this.preparedStatement.setInt(1, id);
			this.resultSet = this.preparedStatement.executeQuery();
			while (this.resultSet.next()) {
				files.setId				(this.resultSet.getInt("id"));
				files.setName	(this.resultSet.getString("name"));
				files.setSize_mb		(this.resultSet.getFloat("size_mb"));
				files.setStatus		(this.resultSet.getString("status"));
				//files.setFile			(this.resultSet.getBlob("file"));
				//files.setUpdated			(this.resultSet.getTimestamp("updated"));			        
				files.setUser_added		(this.resultSet.getInt("user_added"));
				files.setUser_updated		(this.resultSet.getInt("user_updated"));
				//files.setAdded			(this.resultSet.getTimestamp("added"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("SQLException get file by id: " + e.getMessage() + " - " + e.toString());
		} catch (Exception e) {
		  System.err.println("Got an exception get file by id: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
  	  	return files;
  	}
}
