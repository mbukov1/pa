package dao;

import java.text.*;
import java.util.*;

import model.UserBean;
import util.ConnectionManager;
import util.QueryUtil;

import java.sql.*;

public class UserDAO {
	
	private static PreparedStatement preparedStatement = null;
	private static Connection localConn = null;
	private static ResultSet resultSet = null;

	public static UserBean login(UserBean bean) {

		String username = bean.getUserName();
		String password = bean.getPassword();
		
		String searchQuery = "SELECT * FROM pa.users WHERE username = '" + username + "' AND password = '" + password + "'";

		try {
			// connect to DB
			localConn = QueryUtil.getLocalConnection();

			preparedStatement = localConn.prepareStatement(searchQuery);

			resultSet = preparedStatement.executeQuery();

			boolean more = resultSet.next();

			// if user does not exist set the isValid variable to false
			if (!more) {
				System.out.println("Sorry, you are not a registered user! Please sign up first");
				bean.setValid(false);
			}

			// if user exists set the isValid variable to true
			else if (more) {
				String user_name = resultSet.getString("username");
				String pass_word = resultSet.getString("password");

				System.out.println("Welcome " + user_name);
				bean.setUserName(user_name);
				bean.setPassword(pass_word);
				bean.setValid(true);
			}
		}

		catch (Exception ex) {
			System.out.println("Log In failed: An Exception has occurred! " + ex);
		}

		// some exception handling
		finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (Exception e) {
				}
				resultSet = null;
			}
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (Exception e) {
				}
				preparedStatement = null;
			}
			if (localConn != null) {
				try {
					localConn.close();
				} catch (Exception e) {
				}
				localConn = null;
			}
		}
		return bean;
	}
}
