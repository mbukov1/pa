package dao;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import model.DeviceLog;
import util.QueryUtil;

public class DeviceLogDAO extends QueryUtil {
	
	private PreparedStatement preparedStatement = null;
	private Connection localConn = null;
	private ResultSet resultSet = null;
	Timestamp ts = new Timestamp(0);
	
	public int insertDeviceLog(DeviceLog log) throws IOException {
		int returnValue = -1;
		this.localConn = QueryUtil.getLocalConnection();
		String sql = "INSERT INTO pa.device_logs " + 
				"(mac_address, file_name, play_start, play_end, screen_size, battery, connection_number, charging, latitude, longitude, created, updated, description) " + 
				"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try {
			this.preparedStatement = this.localConn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			this.preparedStatement.setString(1, log.getMacAddress());
			this.preparedStatement.setString(2, log.getFileName());
			//this.preparedStatement.setDate(3, new java.sql.Date(log.getPlayStart().getTime()));
			//this.preparedStatement.setDate(4, new java.sql.Date(log.getPlayEnd().getTime()));
			this.preparedStatement.setTimestamp(3, (Timestamp) log.getPlayStart());
			this.preparedStatement.setTimestamp(4, (Timestamp) log.getPlayEnd());
			this.preparedStatement.setString(5, log.getScreenSize());
			this.preparedStatement.setDouble(6, log.getBattery());
			this.preparedStatement.setInt(7, log.getConnectionNumber());
			this.preparedStatement.setString(8, log.getCharging());
			this.preparedStatement.setDouble(9, log.getLatitude());
			this.preparedStatement.setDouble(10, log.getLongitude());
			this.preparedStatement.setTimestamp(11, (Timestamp) log.getCreated());
			this.preparedStatement.setTimestamp(12, (Timestamp) log.getUpdated());
			this.preparedStatement.setString(13, log.getDescription());
			//this.preparedStatement.setDate(11, new java.sql.Date(log.getCreated().getTime()));
			//this.preparedStatement.setDate(12, new java.sql.Date(log.getUpdated().getTime()));
			this.preparedStatement.executeUpdate();
	    	ResultSet keys = this.preparedStatement.getGeneratedKeys();
    	    if (keys.next()) {
    	    	returnValue = keys.getInt(1); //id returned after insert execution
    	        //System.out.println("returnValue = " + returnValue);
    	    } 
		} catch (Exception e) {
		  System.err.println("Got an exception insert file: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
		System.out.println("return = " + returnValue);
  	  	return returnValue;
	}
  
	public void updateFile(DeviceLog log) throws IOException {
		
		this.localConn = QueryUtil.getLocalConnection();
		
		String sql ="UPDATE pa.device_logs " + 
					"SET mac_address=?, file_name=?, play_start=?, play_end=?, screen_size=?, battery=?, connection_number=?, charging=?, latitude=?, longitude=?, created=?, updated=?, description=? " + 
					"WHERE id=?";

		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			
			this.preparedStatement.setString(1, log.getMacAddress());
			this.preparedStatement.setString(2, log.getFileName());
			this.preparedStatement.setDate(3, new java.sql.Date(log.getPlayStart().getTime()));
			this.preparedStatement.setDate(4, new java.sql.Date(log.getPlayEnd().getTime()));
			this.preparedStatement.setString(5, log.getScreenSize());
			this.preparedStatement.setDouble(6, log.getBattery());
			this.preparedStatement.setInt(7, log.getConnectionNumber());
			this.preparedStatement.setString(8, log.getCharging());
			this.preparedStatement.setDouble(9, log.getLatitude());
			this.preparedStatement.setDouble(10, log.getLongitude());
			this.preparedStatement.setDate(11, new java.sql.Date(log.getCreated().getTime()));
			this.preparedStatement.setDate(12, new java.sql.Date(log.getUpdated().getTime()));
			this.preparedStatement.setString(13, log.getDescription());
			this.preparedStatement.setLong(14, log.getId());
			
			this.preparedStatement.executeUpdate();
			
		} catch (Exception e) {
		  System.err.println("Update file got an exception: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Update file: Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
  	}
  
  	public void deleteFile(Long logId) throws IOException {
  		
		this.localConn = QueryUtil.getLocalConnection();
		
		String sql = "DELETE FROM pa.device_logs " +
					 "WHERE id = ? ";
		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			this.preparedStatement.setLong(1, logId);
			this.preparedStatement.executeUpdate();
		} catch (Exception e) {
		  System.err.println("Delete file got an exception: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Delete file got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
  	}
  	
  	public DeviceLog getFileById(int id) throws IOException {
  		DeviceLog log = new DeviceLog();
  		
		this.localConn = QueryUtil.getLocalConnection();
		String sql = "SELECT id, mac_address, file_name, play_start, play_end, screen_size, battery, connection_number, charging, latitude, longitude, created, updated, description "
				+ "FROM pa.device_logs WHERE id = ?";
		
		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			this.preparedStatement.setInt(1, id);
			this.resultSet = this.preparedStatement.executeQuery();
			while (this.resultSet.next()) {
				log.setId				(this.resultSet.getLong("id"));
				log.setMacAddress		(this.resultSet.getString("mac_address"));
				log.setFileName			(this.resultSet.getString("file_name"));
				log.setPlayStart		(this.resultSet.getTimestamp("play_start"));
				log.setPlayEnd			(this.resultSet.getTimestamp("play_end"));
				log.setScreenSize		(this.resultSet.getString("screen_size"));
				log.setBattery			(this.resultSet.getDouble("battery"));
				log.setConnectionNumber	(this.resultSet.getInt("connection_number"));
				log.setCharging			(this.resultSet.getString("charging"));
				log.setLatitude			(this.resultSet.getDouble("latitude"));
				log.setLongitude		(this.resultSet.getDouble("longitude"));
				log.setCreated			(this.resultSet.getTimestamp("created"));
				log.setUpdated			(this.resultSet.getTimestamp("updated"));
				log.setDescription		(this.resultSet.getString("description"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("SQLException get file by id: " + e.getMessage() + " - " + e.toString());
		} catch (Exception e) {
		  System.err.println("Got an exception get file by id: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
  	  	return log;
  	} 	
}
