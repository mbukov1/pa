package dao;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

import model.SchedulerExchange;
import util.QueryUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SchedulerExchangeDAO extends QueryUtil {
	private PreparedStatement preparedStatement = null;
	private Connection localConn = null;
	private ResultSet resultSet = null;
	Timestamp ts = new Timestamp(0);
	
	public int insertSchedulerExchange(SchedulerExchange dataExchange) throws IOException {
		int returnValue = -1;
		this.localConn = QueryUtil.getLocalConnection();
		String sql = "INSERT INTO scheduler_exchange (location_id, device_id, prepared, taken, updated) " + 
					 "VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP)";
		try {
			this.preparedStatement = this.localConn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			this.preparedStatement.setLong(1, dataExchange.getLocationId());
			this.preparedStatement.setLong(2, dataExchange.getDeviceId());
			this.preparedStatement.setTimestamp(3, (Timestamp) dataExchange.getPrepared());
			this.preparedStatement.setTimestamp(4, (Timestamp) dataExchange.getTaken());
			this.preparedStatement.executeUpdate();
	    	ResultSet keys = this.preparedStatement.getGeneratedKeys();
    	    if (keys.next()) {
    	    	returnValue = keys.getInt(1); //id returned after insert execution
    	        //System.out.println("returnValue = " + returnValue);
    	    } 
		} catch (Exception e) {
		  System.err.println("Got an exception insert file: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
		System.out.println("return = " + returnValue);
  	  	return returnValue;
	}
  
	public void updateSchedulerExchange(SchedulerExchange dataExchange) throws IOException {
		this.localConn = QueryUtil.getLocalConnection();
		
		String sql ="UPDATE scheduler_exchange " + 
					"SET location_id=?, device_id=?, prepared=?, taken=?, updated=CURRENT_TIMESTAMP " + 
					"WHERE id=?";

		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			this.preparedStatement.setLong(1, dataExchange.getLocationId());
			this.preparedStatement.setLong(2, dataExchange.getDeviceId());
			this.preparedStatement.setDate(3, new java.sql.Date(dataExchange.getPrepared().getTime()));
			if (dataExchange.getTaken() == null) {
				this.preparedStatement.setDate(4, null);
			} else {
				this.preparedStatement.setDate(4, new java.sql.Date(dataExchange.getTaken().getTime()));
			}
			this.preparedStatement.setLong(5, dataExchange.getId());
			
			this.preparedStatement.executeUpdate();
			
			
		} catch (Exception e) {
		  System.err.println("Update data_echange got an exception: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Update data_echange: Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
  	}
  
  	public void deleteSchedulerExchange(Long logId) throws IOException {
		this.localConn = QueryUtil.getLocalConnection();
		String sql = "DELETE FROM pa.scheduler_exchange " +
					 "WHERE id = ? ";
		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			this.preparedStatement.setLong(1, logId);
			this.preparedStatement.executeUpdate();
		} catch (Exception e) {
		  System.err.println("Delete scheduler_exchange got an exception: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Delete scheduler_exchange got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
  	}
  	
  	public SchedulerExchange getSchedulerExchangeById(int id) throws IOException {
  		SchedulerExchange log = new SchedulerExchange();
  		
		this.localConn = QueryUtil.getLocalConnection();
		String sql = "SELECT id, location_id, device_id, prepared, taken, updated "
				+ "FROM pa.scheduler_exchange WHERE id = ?";
		
		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			this.preparedStatement.setInt(1, id);
			this.resultSet = this.preparedStatement.executeQuery();
			while (this.resultSet.next()) {
				log.setId				(this.resultSet.getLong("id"));
				log.setLocationId		(this.resultSet.getLong("location_id"));
				log.setDeviceId			(this.resultSet.getLong("device_id"));
				log.setPrepared			(this.resultSet.getTimestamp("prepared"));
				log.setTaken			(this.resultSet.getTimestamp("taken"));
				log.setUpdated			(this.resultSet.getTimestamp("updated"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("SQLException get file by id: " + e.getMessage() + " - " + e.toString());
		} catch (Exception e) {
		  System.err.println("Got an exception get file by id: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
		System.out.println("Id: " + log.getId());
		
  	  	return log;
  	} 	
  	
  	public void schedulerExchangeTaken(String mac) throws IOException {
		if (mac != null && mac.length() > 0) {
			this.localConn = QueryUtil.getLocalConnection();
		
			String sql ="UPDATE scheduler_exchange e " + 
						"SET e.taken=CURRENT_TIMESTAMP, e.updated=CURRENT_TIMESTAMP " + 
						"WHERE e.device_id = (select d.id from devices d where d.mac = ?)";
	
			try {
				this.preparedStatement = this.localConn.prepareStatement(sql);
				this.preparedStatement.setString(1, mac);
				this.preparedStatement.executeUpdate();
			} catch (Exception e) {
			  System.err.println("Update data_echange by mac got an exception: " + e.getMessage() + " - " + e.toString());
			}
			finally {
				try {
					this.localConn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					System.err.println("Update data_echange by mac: Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
				}
			}
		}
  	}
  	
  	public static void main(String[] args) {
  		try {
			System.out.println(new SchedulerExchangeDAO().getSchedulerExchangeById(1).getDeviceId());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  		SchedulerExchangeDAO dao = new SchedulerExchangeDAO();
  		
  		SchedulerExchange se = new SchedulerExchange();
//  		se.setLocationId(Long.valueOf("1"));
//  		se.setDeviceId(Long.valueOf("1"));
//  		se.setPrepared(new Timestamp(new Date().getTime()));
//  		se.setTaken(new Timestamp(new Date().getTime()));
//  		se.setUpdated(new Timestamp(new Date().getTime()));
  		
  		try {
			dao.schedulerExchangeTaken("84:20:96:19:d1:bd");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  		//System.out.println("Insertan id = " + dao.insertSchedulerExchange(se));
//  		dao.deleteSchedulerExchange(Long.valueOf("1"));
	}
}
