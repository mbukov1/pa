package dao;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Properties;

import util.QueryUtil;

public class StatusDeviceDAO {
	private PreparedStatement preparedStatement = null;
	private Connection localConn = null;
	private ResultSet resultSet = null;
	Timestamp ts = new Timestamp(0);
	
	public static String mac_address = null;
	public static String creation_time = null;
	public static String last_updated = null;
	public static String list_of_files = null;
	public static String user = null; 
	public static String status = null;
	
	public int insertStatusOfDevice(String mac_address, String creation_time, String last_updated, String list_of_files, String user, String status) throws IOException {
		int returnValue = -1;
		this.localConn = QueryUtil.getLocalConnection();
		String sql = "INSERT INTO pa.status_of_device " + 
				"(mac_address, creation_time, last_updated, list_of_files, user, status) " + 
				"VALUES (?, ?, ?, ?, ?, ?)";
		try {
			this.preparedStatement = this.localConn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			this.preparedStatement.setString(1, mac_address);
			this.preparedStatement.setString(2, creation_time);
			this.preparedStatement.setString(3, last_updated);
			this.preparedStatement.setString(4, list_of_files);
			this.preparedStatement.setString(5, user);
			this.preparedStatement.setString(6, status);
			
			this.preparedStatement.executeUpdate();
	    	ResultSet keys = this.preparedStatement.getGeneratedKeys();
    	    if (keys.next()) {
    	    	returnValue = keys.getInt(1); //id returned after insert execution
    	        //System.out.println("returnValue = " + returnValue);
    	    } 
		} catch (Exception e) {
		  System.err.println("Got an exception inserting status of device: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
		System.out.println("return = " + returnValue);
  	  	return returnValue;
	}
	
	public String updateStatusOfDevice(String mac_address, String creation_time, String last_updated, String list_of_files, String user, String status) throws IOException {
		
		String return_value = "-1";
		this.localConn = QueryUtil.getLocalConnection();
		
		String sql ="UPDATE pa.status_of_device " + 
					"SET mac_address=?, last_updated=?, list_of_files=?, user=?, status=? " + 
					"WHERE creation_time=?";

		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			this.preparedStatement.setString(1, mac_address);
			this.preparedStatement.setString(2, last_updated);
			this.preparedStatement.setString(3, list_of_files);
			this.preparedStatement.setString(4, user);
			this.preparedStatement.setString(5, status);

			this.preparedStatement.setString(6, creation_time);
			
			this.preparedStatement.executeUpdate();
			
			return_value = "OK";
		} catch (Exception e) {
		  System.err.println("Update status_of_device got an exception: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Update status_of_device: Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
		return return_value;
  	}
	
	public String getStatusOfDeviceByMacAddress(String mac_address) throws IOException {
		int return_id = -1;
		String date_created = "-1";
		this.localConn = QueryUtil.getLocalConnection();
		String sql = "SELECT id, creation_time "
				+ "FROM pa.status_of_device WHERE mac_address = ?";
		
		try {
			this.preparedStatement = this.localConn.prepareStatement(sql);
			this.preparedStatement.setString(1, mac_address);
			this.resultSet = this.preparedStatement.executeQuery();
			while (this.resultSet.next()) {
				return_id = this.resultSet.getInt("id");
				date_created = this.resultSet.getString("creation_time");
				System.out.println("return_id: " + return_id);
				System.out.println("date_created: " + date_created);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("SQLException get file by return id: " + e.getMessage() + " - " + e.toString());
		} catch (Exception e) {
		  System.err.println("Got an exception get file by return id: " + e.getMessage() + " - " + e.toString());
		}
		finally {
			try {
				this.localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Got an exception close local connection: " + e.getMessage() + " - " + e.toString());
			}
		}
  	  	return date_created;
  	}
	
	public static void saveProjectProperty(String mac_address, String creation_time, String last_updated, String list_of_files, String user, String status)
	{
		Properties prop = new Properties();
		OutputStream output = null;

		try {

			output = new FileOutputStream("config.properties");

			// set the properties value
			prop.setProperty("mac_address", mac_address);
			prop.setProperty("creation_time", creation_time);
			prop.setProperty("last_updated", last_updated);
			prop.setProperty("list_of_files", list_of_files);
			prop.setProperty("user", user);
			prop.setProperty("status", status);

			// save properties to project root folder
			prop.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public static void getProjectProperty()
	{
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("config.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			mac_address = prop.getProperty("mac_address");
			creation_time = prop.getProperty("creation_time");
			last_updated = prop.getProperty("last_updated");
			list_of_files = prop.getProperty("list_of_files");
			user = prop.getProperty("user");
			status = prop.getProperty("status");
			
			System.out.println(prop.getProperty("mac_address"));
			System.out.println(prop.getProperty("creation_time"));
			System.out.println(prop.getProperty("last_updated"));
			System.out.println(prop.getProperty("list_of_files"));
			System.out.println(prop.getProperty("user"));
			System.out.println(prop.getProperty("status"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
