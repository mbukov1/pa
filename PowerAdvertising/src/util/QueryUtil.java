package util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import pa.GetPropertyValues;

public class QueryUtil {
  static final Logger logger = Logger.getLogger(QueryUtil.class);

  private static String MariaDB_DRIVER = "";
  public static String MariaDB_URL = "";
  public static GetPropertyValues properties = null;
  public static Connection localConn = null;
  
  public static Map<String, String> splitQuery(String query) throws UnsupportedEncodingException {
	Map<String, String> query_pairs = new LinkedHashMap();
    String[] pairs = query.split("&");
    for (String pair : pairs) {
      int idx = pair.indexOf("=");
      query_pairs.put(pair.substring(0, idx), pair.substring(idx + 1));
    }
    return query_pairs;
  }
  
  @SuppressWarnings("finally")
public static Connection getLocalConnection() throws IOException {
		System.out.println("Method getLocalConnection started!");
		
		try {
			properties = new GetPropertyValues();
			properties.getPropValues();
			
			MariaDB_DRIVER = GetPropertyValues.getMariaDB_DRIVER();
			MariaDB_URL = GetPropertyValues.getMariaDBPath();
			
			Class.forName(MariaDB_DRIVER);
			localConn = DriverManager.getConnection(MariaDB_URL);
			System.out.println("Method getLocalConnection executed successfully!");

		} catch (Exception e) {
			System.err.println("Got an exception: getLocalConnection MariaDB_URL! ");
			System.err.println(e.toString());
		}		
		finally {
		return localConn;
		}
	}
}
