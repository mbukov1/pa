package util;

import java.awt.Desktop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import model.Files;
import model.SchedulerFiles;
import pa.PAServletJsonArray;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParser;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.sql.Blob;

public class JSONUtil {
	static final Logger logger = Logger.getLogger(JSONUtil.class);

	public static ArrayList<Files> getFilesJSONList() {
		System.out.println("Method getFilesJSONList started!");
		String query = "SELECT id, name, size_mb, status, updated, user_added, user_updated, added, file_type FROM pa.files";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;		
		Files file = null;
		Connection localConn = null;
		ArrayList<Files> fileArrayList = new ArrayList();
		File someFile = null;
		try {		
			localConn = QueryUtil.getLocalConnection();
			preparedStatement = localConn.prepareStatement(query);
;
			resultSet = preparedStatement.executeQuery(query);
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				double size_mb = resultSet.getFloat("size_mb");
				String status = resultSet.getString("status");
				String file_type = resultSet.getString("file_type");
				//byte[] files = resultSet.getBytes("file");
				/*if(file_type.contains("Video")) {				
				someFile = new File(name + ".mp4");
		        FileOutputStream fos = new FileOutputStream(someFile);
		        System.out.println("someFile mp4: " + someFile);
		        fos.write(files);
		        fos.flush();
		        fos.close();		
				}
				else {*/
				/*if(file_type.contains("Image")) {
					Blob files1 = resultSet.getBlob("file"); 
					byte[] files = convertToFile(files1);
					files1.free();
					
					someFile = new File(name + ".png");
			        FileOutputStream fos = new FileOutputStream(someFile);
			        System.out.println("someFile png: " + someFile);
			        fos.write(files);
			        fos.flush();
			        fos.close();
				}*/
				
				String updated = String.valueOf(resultSet.getTimestamp("updated"));
				int user_added = resultSet.getInt("user_added");
				int user_updated = resultSet.getInt("user_updated");
				String added = String.valueOf(resultSet.getTimestamp("added"));
				
				
				//file = new Files(id, name, size_mb, status, updated, user_added, user_updated, added, file_type);
				//Files file = new Files(id, name, size_mb, status, user_added, user_updated);
				//fileArrayList.add(file);	
				
				/*System.out.println("Id: " + file.getId());	
				System.out.println("Name: " + file.getName());
				System.out.println("status: " + file.getStatus());
				System.out.println("size_mb: " + file.getSize_mb());
				System.out.println("files: " + file.getFile());
				System.out.println("updated: " + file.getUpdated());
				System.out.println("user_added: " + file.getUser_added());
				System.out.println("user_updated: " + file.getUser_updated());
				System.out.println("added: " + file.getAdded());*/
				
				//System.out.println("fileArrayList: " + fileArrayList);
			}
			try {
				resultSet.close();
				preparedStatement.close();
				localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			System.out.println("Method getServerJSONList executed!");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return fileArrayList;
	}
	
	public static byte[] convertToFile(Blob file) {		
		byte[] files = null;
		try {
			int blobLength = (int) file.length();  
			files = file.getBytes(1, blobLength);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return files;
	}
	
	public static ArrayList<String> noDupsDates(ArrayList<String> arr){

	    int j = 0;
	    // copy the items without the dups to res
	    ArrayList<String> res = new ArrayList<String>(arr.size());
	    for(int i=0; i<arr.size()-2; i++){
	        if(!arr.get(i).equals(arr.get(i+1))) {
	            res.add(j, arr.get(i));
	            j++;
	        }
	    }
	    // copy the last element
	    res.add(j, arr.get(arr.size()-1));
	    j++;
	    // now move the result into a compact array (exact size)
	    ArrayList<String> ans = new ArrayList<String>(j);
	    for(int i=0; i<j; i++){
	        ans.add(i, res.get(i));
	    }
	    /*for(int i=0; i<j; i++){
	    	System.out.println("Ans: " + ans.get(i));
	    }*/
	    return ans;
	}
	
	public static ArrayList<SchedulerFiles> getScheduler_files(String mac) {

		String query =  "select f.day, f.file_id, f.play_order, f.from_time, f.to_time, f.name, f.size_mb, f.file_type, f.size, f.duration " +
						"from scheduler_files f " +
						"where not exists (select 1 from scheduler_exchange se, devices d where se.device_id = d.id and d.status = 'A' " +
						"and se.taken > se.prepared " + 
						"and lower(d.mac) = '" + mac + "') " +
						//"and lower(d.mac) = ? ) " +
						"order by f.day, f.from_time, f.play_order";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;		
		Connection localConn = null;
		ArrayList<SchedulerFiles> scheduler_filesArrayList = new ArrayList();
		SchedulerFiles scheduler_files = null;
		mac = mac.toLowerCase();
		try {		
			localConn = QueryUtil.getLocalConnection();
			
			preparedStatement = localConn.prepareStatement(query);
			//preparedStatement.setString(1, mac);
			resultSet = preparedStatement.executeQuery(query);
			
			
			while (resultSet.next()) {				
				
				String day = resultSet.getString("day");
				int file_id = resultSet.getInt("file_id");
				int play_order = resultSet.getInt("play_order");
				String from_time = resultSet.getString("from_time");
				String to_time = resultSet.getString("to_time");
				String name = resultSet.getString("name");
				double size_mb = resultSet.getFloat("size_mb"); 
				String file_type = resultSet.getString("file_type");
				String size = resultSet.getString("size");
                int duration = resultSet.getInt("duration");

                scheduler_files = new SchedulerFiles(day, file_id, play_order, from_time, to_time, name, size_mb, file_type, size, duration);
                scheduler_filesArrayList.add(scheduler_files);
                
                System.out.println("day: " + day);	
				System.out.println("file_id: " + file_id);	
				System.out.println("play_order: " + play_order);
				System.out.println("from_time: " + from_time);
				System.out.println("to_time: " + to_time);
				System.out.println("name: " + name);
				System.out.println("size_mb: " + size_mb);
				System.out.println("file_type: " + file_type);
				System.out.println("size: " + size);
				System.out.println("duration: " + duration);
			}
		}
		catch(Exception e) {
			System.out.println(e.toString());
		}
		finally {
			try {
				resultSet.close();
				preparedStatement.close();
				localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return scheduler_filesArrayList;
	}
	
	public static File convert() {
		String query = "SELECT name, file FROM pa.files WHERE name = 'Mobile'";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;		
		Files file = null;
		Connection localConn = null;
		ArrayList<Files> fileArrayList = new ArrayList();
		//java.util.List<java.io.File> filesList = new ArrayList<>();
		java.util.List<java.io.File> filesList = new ArrayList<java.io.File>();
		File someFile = null;
		try {		
			localConn = QueryUtil.getLocalConnection();
			preparedStatement = localConn.prepareStatement(query);
			resultSet = preparedStatement.executeQuery(query);
			
			while (resultSet.next()) {				
				String name = resultSet.getString("name");			

				Blob files1 = resultSet.getBlob("file"); 
				byte[] files = convertToFile(files1);
				files1.free();
				
				someFile = new File(name + ".png");
				FileOutputStream fos = new FileOutputStream(someFile);
		        System.out.println("someFile png: " + someFile);
		        fos.write(files);
		        fos.flush();
		        fos.close();
		        filesList.add(someFile);
			}
		}
		        catch (Exception e) {
					e.printStackTrace();
				}
			finally {
				try {
					resultSet.close();
					preparedStatement.close();
					localConn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}    
		return someFile;
	}
	
	public static java.util.List<java.io.File> convertFromBytes(ArrayList arrayList) {
		String query = "SELECT name, file, file_type FROM pa.files";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;		
		Files file = null;
		Connection localConn = null;
		ArrayList<Files> fileArrayList = new ArrayList();
		//java.util.List<java.io.File> filesList = new ArrayList<>();
		java.util.List<java.io.File> filesList = new ArrayList<java.io.File>();
		File someFile = null;
		try {		
			localConn = QueryUtil.getLocalConnection();
			preparedStatement = localConn.prepareStatement(query);
;
			resultSet = preparedStatement.executeQuery(query);
			for(int j = 0; j < arrayList.size(); j++) {
			while (resultSet.next()) {
				
				String name = resultSet.getString("name");
				String file_type = resultSet.getString("file_type");			

				if(file_type.contains("Video")) {
					Blob files1 = resultSet.getBlob("file"); 
					byte[] files = convertToFile(files1);
					files1.free();
					
					someFile = new File(name + ".mp4");	
					FileOutputStream fos = new FileOutputStream(someFile);
			        System.out.println("someFile mp4: " + someFile);
			        fos.write(files);
			        fos.flush();
			        fos.close();
			        filesList.add(someFile);
			        //j++;
					}
					else {
					//if(file_type.contains("Image")) {
						Blob files1 = resultSet.getBlob("file"); 
						byte[] files = convertToFile(files1);
						files1.free();
						
						someFile = new File(name + ".png");
						FileOutputStream fos = new FileOutputStream(someFile);
				        System.out.println("someFile mp4: " + someFile);
				        fos.write(files);
				        fos.flush();
				        fos.close();
				        filesList.add(someFile);
				        //filesList[j] = someFile;
				        //j++;
				        //filesList.add(someFile);
					}
				}
				//System.out.println(files1.toString());
			}
		}
			catch (Exception e) {
				e.printStackTrace();
			}
		finally {
			try {
				resultSet.close();
				preparedStatement.close();
				localConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		/*String listString = (String) arrayList.stream().map(Object::toString)
                .collect(Collectors.joining(", "));
		
		//JSONParser jsonParser = new JSONParser();
		// Convert JSON Array String into JSON Array 
		//String jsonArrayString = "[\"Russian\",\"English\",\"French\"]";
		//JSONArray arrayFromString = (JSONArray) jsonParser.parse(listString);
		
		
		/*String data=listString;
		Object object=null;
		JSONArray arrayObj=null;
		JSONParser jsonParser=new JSONParser();
		object=jsonParser.parse(arrayList.toString());
		arrayObj=(JSONArray) object;
		System.out.println("Json object :: "+arrayObj);
		
		JSONObject obj=new JSONObject();
		obj.put("status",arrayObj);*/
		
		/*List<Object> list = new ArrayList<Object>();

        for (int i = 0; i < arrayList.size(); i++) {
            try {
                Object obj = arrayList.get(i);

                System.out.println("obj:" + obj);

            }
            catch (JSONException jsone) {
                //Log.wtf("RequestManager", "Failed to get value at index " + i + " from JSONArray.", jsone);
            }
        }*/

		
		//System.out.println("Array list string: " + jsonArray);
	
        /*for (int i = 0; i < obj.size(); i++) {
			JSONObject object = (JSONObject) jsonArray.getJSONObject(i);
			JSONObject object = (JSONObject) arrayObj.getJSONObject(i);
			//System.out.println("Array: " + obj.get("result"));
		}*/
		return filesList;
	}
	
	public static void openFile(java.util.List<java.io.File> arrayList) {
		for(int f = 0; f < arrayList.size(); f++) {
			
			System.out.println("Array list: " + arrayList.get(f));
		
		Desktop desktop = Desktop.getDesktop();
        if(((File) arrayList.get(f)).exists()) { 
        	try {
				desktop.open((File) arrayList.get(f));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		}	
	}
	
	public static ArrayList<SchedulerFiles> todaysFiles(ArrayList<SchedulerFiles> arraylist) {
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date today = Calendar.getInstance().getTime();        
		String reportDate = df.format(today);
		
		ArrayList<SchedulerFiles> todaylistfiles = new ArrayList();
		SchedulerFiles s_f_today = null;
		
		for(int h = 0; h<arraylist.size(); h++) { 
			//System.out.println("arraylist: " +arraylist.get(h));
			if(arraylist.get(h).getDay().equals(reportDate)) {
				String day = arraylist.get(h).getDay();
				int file_id = arraylist.get(h).getFile_id();
				int play_order = arraylist.get(h).getPlay_order();
				String from_time = arraylist.get(h).getFrom_time();
				String to_time = arraylist.get(h).getTo_time();
				String name = arraylist.get(h).getName();
				double size_mb = arraylist.get(h).getSize_mb(); 
				String file_type = arraylist.get(h).getFile_type();
				String size = arraylist.get(h).getSize();
				int duration = arraylist.get(h).getDuration();
				
				s_f_today = new SchedulerFiles(day, file_id, play_order, from_time, to_time, name, size_mb, file_type, size, duration);
				todaylistfiles.add(s_f_today);
				
				System.out.println("day: " + day);	
				System.out.println("file_id: " + file_id);	
				System.out.println("play_order: " + play_order);
				System.out.println("from_time: " + from_time);
				System.out.println("to_time: " + to_time);
				System.out.println("name: " + name);
				System.out.println("size_mb: " + size_mb);
				System.out.println("file_type: " + file_type);
				System.out.println("size: " + size);
				System.out.println("duration: " + duration);
			}	
		}
		return todaylistfiles;
	}
	
	public static void main(String[] args) throws ParseException, IOException, IOException, ClassNotFoundException { 
		//System.out.println(JSONUtil.getServerJSONList(con));
		/*ArrayList arrayList = getFilesJSONList();
		java.util.List<java.io.File> aList = convertFromBytes(arrayList);
		openFile(aList);
		/*File aList = convert();
		Desktop desktop = Desktop.getDesktop();
		desktop.open(aList);*/
		//System.out.println("arrayList: " + arrayList);
		//System.out.println("arrayList: " + convertFromBytes(arrayList));
		
		/*ObjectOutputStream outFile = new ObjectOutputStream(new FileOutputStream(new File("list_of_files")));
	    outFile.writeObject(aList);
	    outFile.close();

	    ObjectInputStream inFile = new ObjectInputStream(new FileInputStream(new File("list_of_files")));
	    java.util.List<java.io.File> list2 = (List<File>) inFile.readObject();
	    System.out.println("list:" + list2);
	    openFile(list2);*/
		
		ArrayList<SchedulerFiles> arraylist = getScheduler_files("84:20:96:19:d1:bd");
		//todaysFiles(arraylist);
		//ArrayList<String> arraystring = getDatesShowing(arraylist);
		//ArrayList<String> arraystringDates = noDupsDates(arraystring);
		
		/*ArrayList<Scheduler_files> todaylistfiles = new ArrayList();
		
		ArrayList<Scheduler_files> todaylistfiles00h = new ArrayList();
		ArrayList<Scheduler_files> todaylistfiles07h = new ArrayList();
		ArrayList<Scheduler_files> todaylistfiles12h = new ArrayList();
		ArrayList<Scheduler_files> todaylistfiles17h = new ArrayList();
		
		Scheduler_files s_f_today = null;
		Scheduler_files s_f_today00h = null;
		Scheduler_files s_f_today07h = null;
		Scheduler_files s_f_today12h = null;
		Scheduler_files s_f_today17h = null;
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		// Get the date today using Calendar object.
		Date today = Calendar.getInstance().getTime();        
		// Using DateFormat format method we can create a string 
		// representation of a date with the defined format.
		String reportDate = df.format(today);

		// Print what date is today!
		System.out.println("Today's Date: " + reportDate);
		
		for(int h = 0; h<arraylist.size(); h++) { 
			//System.out.println("arraylist: " +arraylist.get(h));
			if(arraylist.get(h).getDay().equals(reportDate)) {
				String day = arraylist.get(h).getDay();
				int file_id = arraylist.get(h).getFile_id();
				int play_order = arraylist.get(h).getPlay_order();
				String from_time = arraylist.get(h).getFrom_time();
				String to_time = arraylist.get(h).getTo_time();
				String name = arraylist.get(h).getName();
				double size_mb = arraylist.get(h).getSize_mb(); 
				String file_type = arraylist.get(h).getFile_type();
				
				s_f_today = new Scheduler_files(day, file_id, play_order, from_time, to_time, name, size_mb, file_type);
				todaylistfiles.add(s_f_today);
				
				System.out.println("day: " + day);	
				System.out.println("file_id: " + file_id);	
				System.out.println("play_order: " + play_order);
				System.out.println("from_time: " + from_time);
				System.out.println("to_time: " + to_time);
				System.out.println("name: " + name);
				System.out.println("size_mb: " + size_mb);
				System.out.println("file_type: " + file_type);
			}	
		}
		
		for(int h = 0; h<todaylistfiles.size(); h++) { 
			//System.out.println("todaylistfiles: " +todaylistfiles.get(h));
			if(todaylistfiles.get(h).getFrom_time().equals("00:00:00")) {
				String day = todaylistfiles.get(h).getDay();
				int file_id = todaylistfiles.get(h).getFile_id();
				int play_order = todaylistfiles.get(h).getPlay_order();
				String from_time = todaylistfiles.get(h).getFrom_time();
				String to_time = todaylistfiles.get(h).getTo_time();
				String name = todaylistfiles.get(h).getName();
				double size_mb = todaylistfiles.get(h).getSize_mb(); 
				String file_type = todaylistfiles.get(h).getFile_type();
				
				s_f_today00h = new Scheduler_files(day, file_id, play_order, from_time, to_time, name, size_mb, file_type);
				todaylistfiles00h.add(s_f_today00h);
				
				System.out.println("day: " + day);	
				System.out.println("file_id: " + file_id);	
				System.out.println("play_order: " + play_order);
				System.out.println("from_time: " + from_time);
				System.out.println("to_time: " + to_time);
				System.out.println("name: " + name);
				System.out.println("size_mb: " + size_mb);
				System.out.println("file_type: " + file_type);
			}
			else if(todaylistfiles.get(h).getFrom_time().equals("07:00:00")) {
				String day = todaylistfiles.get(h).getDay();
				int file_id = todaylistfiles.get(h).getFile_id();
				int play_order = todaylistfiles.get(h).getPlay_order();
				String from_time = todaylistfiles.get(h).getFrom_time();
				String to_time = todaylistfiles.get(h).getTo_time();
				String name = todaylistfiles.get(h).getName();
				double size_mb = todaylistfiles.get(h).getSize_mb(); 
				String file_type = todaylistfiles.get(h).getFile_type();
				
				s_f_today07h = new Scheduler_files(day, file_id, play_order, from_time, to_time, name, size_mb, file_type);
				todaylistfiles07h.add(s_f_today07h);
				
				System.out.println("day: " + day);	
				System.out.println("file_id: " + file_id);	
				System.out.println("play_order: " + play_order);
				System.out.println("from_time: " + from_time);
				System.out.println("to_time: " + to_time);
				System.out.println("name: " + name);
				System.out.println("size_mb: " + size_mb);
				System.out.println("file_type: " + file_type);
			}
			else if(todaylistfiles.get(h).getFrom_time().equals("12:00:00")) {
				String day = todaylistfiles.get(h).getDay();
				int file_id = todaylistfiles.get(h).getFile_id();
				int play_order = todaylistfiles.get(h).getPlay_order();
				String from_time = todaylistfiles.get(h).getFrom_time();
				String to_time = todaylistfiles.get(h).getTo_time();
				String name = todaylistfiles.get(h).getName();
				double size_mb = todaylistfiles.get(h).getSize_mb(); 
				String file_type = todaylistfiles.get(h).getFile_type();
				
				s_f_today12h = new Scheduler_files(day, file_id, play_order, from_time, to_time, name, size_mb, file_type);
				todaylistfiles12h.add(s_f_today12h);
				
				System.out.println("day: " + day);	
				System.out.println("file_id: " + file_id);	
				System.out.println("play_order: " + play_order);
				System.out.println("from_time: " + from_time);
				System.out.println("to_time: " + to_time);
				System.out.println("name: " + name);
				System.out.println("size_mb: " + size_mb);
				System.out.println("file_type: " + file_type);
			}
			else if(todaylistfiles.get(h).getFrom_time().equals("17:00:00")) {
				String day = todaylistfiles.get(h).getDay();
				int file_id = todaylistfiles.get(h).getFile_id();
				int play_order = todaylistfiles.get(h).getPlay_order();
				String from_time = todaylistfiles.get(h).getFrom_time();
				String to_time = todaylistfiles.get(h).getTo_time();
				String name = todaylistfiles.get(h).getName();
				double size_mb = todaylistfiles.get(h).getSize_mb(); 
				String file_type = todaylistfiles.get(h).getFile_type();
				
				s_f_today17h = new Scheduler_files(day, file_id, play_order, from_time, to_time, name, size_mb, file_type);
				todaylistfiles17h.add(s_f_today17h);
				
				System.out.println("day: " + day);	
				System.out.println("file_id: " + file_id);	
				System.out.println("play_order: " + play_order);
				System.out.println("from_time: " + from_time);
				System.out.println("to_time: " + to_time);
				System.out.println("name: " + name);
				System.out.println("size_mb: " + size_mb);
				System.out.println("file_type: " + file_type);
			}
		}*/
		
	}
}
