package pa;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Session;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;

import model.Files;
import util.MyForm;

/**
 * Servlet implementation class MyServlet
 */
@WebServlet("/MyServlet")
public class MyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	PaServlet get_file_servlet = new PaServlet();
	private static String MariaDB_DRIVER = "org.mariadb.jdbc.Driver";
	public static String MariaDB_URL = "jdbc:mariadb://212.93.241.142:22506/pa?user=admin&password=admin";
	
	public Files files;
	Connection connection = null;
	/**
	 * Default constructor.
	 */
	public MyServlet() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
			
		try {

			MyForm myForm = new MyForm(); // Prepare bean.

				String mac_address = get_file_servlet.mac_address;
				List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				files = new Files();
				for (FileItem item : items) {

					String fileName1 = FilenameUtils.getName(item.getName());

					System.out.println("Filename -> " + fileName1);
					
					Class.forName(MariaDB_DRIVER);
					//Connection connection = DriverManager.getConnection("jdbc:mysql://" + "127.0.0.1" + ":" + "3306"
					//		+ "/" + "matic" + "?user=" + "root" + "&password=" + "root");
					connection = DriverManager.getConnection(MariaDB_URL);
					
					if(fileName1!=null) {
						if (!item.isFormField()) {
					if (fileName1.contains(".3gp") || fileName1.contains(".mp4") || fileName1.contains(".ts")
							|| fileName1.contains(".webm") || fileName1.contains(".mkv")) {
						
							String file_type_input = "Video";
							String fieldName = item.getFieldName();
							String fileName = item.getName();
							boolean isInMemory = item.isInMemory();
							float sizeInBytes = item.getSize();
							byte[] bytes = item.get();

							Blob blob = new javax.sql.rowset.serial.SerialBlob(bytes);
							
							files.setName(fileName1);
							files.setSize_mb(sizeInBytes);
							files.setStatus("A");
							files.setFile(blob);
							files.setUpdated(getDate());
							files.setUser_added(1);
							files.setUser_updated(1);
							files.setAdded(getDate());
							files.setFile_type(file_type_input);
							// Statement statement=connection.createStatement();
							
							String sql = "INSERT INTO pa.files (name, size_mb, status, file, updated, user_added, user_updated, added, file_type) " + "VALUES (?,?,?,?,?,?,?,?,?) ";

							PreparedStatement preparedStatement = connection.prepareStatement(sql);

							preparedStatement.setString(1, files.getName());
							preparedStatement.setDouble(2, files.getSize_mb());
							preparedStatement.setString(3, files.getStatus());
							preparedStatement.setBlob(4, files.getFile());
							preparedStatement.setTimestamp(5, files.getUpdated());
							preparedStatement.setInt(6, files.getUser_added());
							preparedStatement.setInt(7, files.getUser_updated());
							preparedStatement.setTimestamp(8, files.getAdded());
							preparedStatement.setString(9, files.getFile_type());
							preparedStatement.executeUpdate();

							/*ResultSet keys = preparedStatement.getGeneratedKeys();
							if (keys.next()) {
								int returnValue = keys.getInt(1); // id returned after insert execution
								// System.out.println("returnValue = " + returnValue);
							}*/

							// statement.executeUpdate("insert into files
							// values('"+fileName+"','"+blob+"','"+"Image"+"')");

							System.out.println(fileName1);
							System.out.println(blob);
							System.out.println(file_type_input);

							request.getSession().setAttribute("submitDone","t");
							request.getRequestDispatcher("pa.jsp").forward(request, response);

							// file = new File( filePath + "yourFileName.png");
							// file.createNewFile();
							// fi.write( file );
						} else if (fileName1.contains(".bmp") || fileName1.contains(".gif") || fileName1.contains(".jpg")
								|| fileName1.contains(".png") || fileName1.contains(".webp")) {
							
									String file_type_input = "Image";
									String fieldName = item.getFieldName();
									String fileName = item.getName();
									boolean isInMemory = item.isInMemory();
									float sizeInBytes = item.getSize();
									byte[] bytes = item.get();

									Blob blob = new javax.sql.rowset.serial.SerialBlob(bytes);
								
									files.setName(fileName1);
									files.setSize_mb(sizeInBytes);
									files.setStatus("A");
									files.setFile(blob);
									files.setUpdated(getDate());
									files.setUser_added(1);
									files.setUser_updated(1);
									files.setAdded(getDate());
									files.setFile_type(file_type_input);
									
									// Statement statement=connection.createStatement();

									String sql = "INSERT INTO pa.files (name, size_mb, status, file, updated, user_added, user_updated, added, file_type) " + "VALUES (?,?,?,?,?,?,?,?,?) ";

									PreparedStatement preparedStatement = connection.prepareStatement(sql);

									preparedStatement.setString(1, files.getName());
									preparedStatement.setDouble(2, files.getSize_mb());
									preparedStatement.setString(3, files.getStatus());
									preparedStatement.setBlob(4, files.getFile());
									preparedStatement.setTimestamp(5, files.getUpdated());
									preparedStatement.setInt(6, files.getUser_added());
									preparedStatement.setInt(7, files.getUser_updated());
									preparedStatement.setTimestamp(8, files.getAdded());
									preparedStatement.setString(9, files.getFile_type());
									preparedStatement.executeUpdate();
									
									//preparedStatement.setString(4, "02:00:00:00:00:00");

									/*ResultSet keys = preparedStatement.getGeneratedKeys();
									if (keys.next()) {
										int returnValue = keys.getInt(1); // id returned after insert execution
										// System.out.println("returnValue = " + returnValue);
									}*/

									// statement.executeUpdate("insert into files
									// values('"+fileName+"','"+blob+"','"+"Image"+"')");
									
									
									
									request.getSession().setAttribute("submitDone","t");
									request.getRequestDispatcher("pa.jsp").forward(request, response);
								}
						else {
							request.getSession().setAttribute("submitDone","f");
							request.getRequestDispatcher("pa.jsp").forward(request, response);
						}
							}
						}
					}
			}
			catch (Exception ex) {
				System.out.println(ex);
			}
		finally {
			try {
				request.getSession().setAttribute("submitDone","false");
				request.getRequestDispatcher("pa.jsp");
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	public static Timestamp getDate() {		
        try {
        	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	Date date1 = new Date();
        	Timestamp timestamp = new java.sql.Timestamp(date1.getTime());
        	System.out.println(dateFormat.format(date1));
            
            System.out.println("Current date: " + timestamp);
            return timestamp;
        } catch (Exception e) {
            e.printStackTrace();
            return (Timestamp) null;
        }
    }
	
	 public static void main(String[] args) {
	 //System.out.println(JSONUtil.getServerJSONList(con));
	 //System.out.println(JSONUtil.getFilesJSONList()); 
	 }
	 
}
