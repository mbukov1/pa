package pa;

import java.io.FileNotFoundException;


import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.util.Properties;

import org.apache.log4j.Logger;

import pa.PAServletJsonArray;

public class GetPropertyValues {
	
	static final Logger logger = Logger.getLogger(GetPropertyValues.class);
	static String resultMariaDB = "";
	static String MariaDB_DRIVER = "";
	InputStream inputStream;

	public static Connection connect = null;
	public static String MariaDB_URL = null;

	public String getPropValues() throws IOException {

		try {
			Properties prop = new Properties();
			String propFileName = "config.properties";
			
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

			if (inputStream != null) {
				prop.load(inputStream);
				//prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

			Date time = new Date(System.currentTimeMillis());

			// get the property value and print it out
			String databseMariaDB = prop.getProperty("databseMariaDb");
			String ipMariaDB = prop.getProperty("ipMariaDb");
			String userMariaDB = prop.getProperty("userMariaDb");
			String passwordMariaDB = prop.getProperty("passwordMariaDb");
			String MariaDRIVER_DB = prop.getProperty("MariaDB_DRIVER");

			resultMariaDB = databseMariaDB + ipMariaDB + userMariaDB + passwordMariaDB;

			// QueryUtil.MariaDB_URL = resultMariaDB;
			// MDBAServlet.MysqlDB = resultMysqlDB;

			setMariaDBPath(resultMariaDB);
			setMariaDB_DRIVER(MariaDRIVER_DB);

			MariaDB_URL = getMariaDBPath();

			getLocalConnection();

			// System.out.println(resultMysqlDB);

			// System.out.println(resultMariaDB);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return resultMariaDB;
	}

	public static String getMariaDBPath() {
		return resultMariaDB;
	}

	public void setMariaDBPath(String resultMariaDB) {
		this.resultMariaDB = resultMariaDB;
	}
	
	public static String getMariaDB_DRIVER() {
		return MariaDB_DRIVER;
	}

	public void setMariaDB_DRIVER(String MariaDB_DRIVER) {
		this.MariaDB_DRIVER = MariaDB_DRIVER;
	}

	public static Connection getLocalConnection() {
		System.out.println("Method getLocalConnection started!");
		try {
			System.out.println("Connection " + MariaDB_URL);
			// Class.forName(GetPropertyValues.getMariaDBPath());
			connect = DriverManager.getConnection(MariaDB_URL);

		} catch (Exception e) {
			System.err.println("Got an exception: getLocalConnection MariaDB_URL! ");
			System.err.println(e.toString());
		}
		System.out.println("Method getLocalConnection executed successfully!");
		return connect;
	}

	public static void main(String[] args) throws IOException {
		GetPropertyValues properties = new GetPropertyValues();
		properties.getPropValues();
		
		System.out.println("MariaDB_DRIVER: "+getMariaDB_DRIVER());
	}
}