package pa;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import dao.SchedulerExchangeDAO;
import dao.StatusDeviceDAO;
import model.DeviceLog;
import model.DeviceLogFromJson;
import model.SchedulerFiles;
import util.JSONUtil;

@WebServlet("/PaServlet")
public class PaServlet extends HttpServlet {
	private static final long serialVersionUID = -8034293455123876788L;

	public static String mac_address = null;
	public static String creation_time = null;
	public static String last_updated = null;
	public static String list_of_files = null;
	public static String user = null;
	public static String status = null;
	public static int value_conn = 0;
	public static int value_conn_counter = 0;
	public static int counter_connection = 0;
	
	public static Timer timer = null;
	public static Timer timer1 = null;
	
	public static String received_status = null;
	String log_server = "";
	
	@SerializedName("data")
	public DeviceLog device_log = new DeviceLog();
	@SerializedName("data")
	public ArrayList<DeviceLogFromJson> device_log_Array_List = null;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// response.getOutputStream()
		// response.getOutputStream().println("This Servlet Works");
		ObjectOutputStream writer = new ObjectOutputStream(response.getOutputStream());
		File aList = JSONUtil.convert();

		writer.writeObject(aList);
		writer.flush();
		writer.close();
		System.out.println("File sent!");
	}

	@SuppressWarnings({ "deprecation", "null" })
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
			String requestString = "";

			String line = "";
			while ((line = br.readLine()) != null) {
				requestString += line;
			}
			// request.getInputStream().close();

			String recievedString = new String(requestString);
			System.out.println("recievedString = " + recievedString);

			JSONObject json = new JSONObject(recievedString);
			String oper = "";
			
			try {
				oper = json.getString("oper");
			} catch (Exception e) {
				System.out.println("Error getting oper: " + e.getMessage());
			}
			
			try {
				mac_address = json.getString("mac_address");
			} catch (Exception e) {
				System.out.println("Error getting mac_address: " + e.getMessage());
			}

			switch (oper) {
			case "get_file":
				ObjectOutputStream writer = new ObjectOutputStream(response.getOutputStream());
				ArrayList arrayList = JSONUtil.getFilesJSONList();
				// java.util.List<java.io.File> aList = JSONUtil.convertFromBytes(arrayList);
				// java.util.List<java.io.File> aList = JSONUtil.convertFromBytes(arrayList);
				// String[] strings = aList.stream().toArray(String[]::new);

				// java.util.List<java.io.File> aList = JSONUtil.convert();
				File aList = JSONUtil.convert();

				writer.writeObject(aList);
				writer.flush();
				writer.close();
				break;
			case "scheduler_files":
				ObjectOutputStream writer1 = new ObjectOutputStream(response.getOutputStream());
				ArrayList<SchedulerFiles> arraylist = JSONUtil.getScheduler_files(mac_address);
				Gson gson = new Gson();
				String jsonText = gson.toJson(arraylist);

				writer1.writeObject(jsonText);
				writer1.flush();
				writer1.close();
				break;
			case "taken":
				SchedulerExchangeDAO dao = new SchedulerExchangeDAO();
				dao.schedulerExchangeTaken(mac_address);
				break;
			case "maria_db_info":
				ObjectOutputStream writer2 = new ObjectOutputStream(response.getOutputStream());
				try {
					GetPropertyValues properties = null;
					properties = new GetPropertyValues();
					properties.getPropValues();
					
					String MariaDB_URL = GetPropertyValues.getMariaDBPath();
					
					writer2.writeObject(MariaDB_URL);
					writer2.flush();
					writer2.close();
				} catch(Exception e) {
					System.out.println("Error getting info maria_db: " + e.getMessage());
				}				
				break;
			case "status_of_device":
				System.out.println(json.getString("counter"));
				counter_connection = Integer.parseInt(json.getString("counter"));
				received_status = json.getString("status");
				value_conn = value_conn + counter_connection;
				
				System.out.println("Value of conn: "+ String.valueOf(value_conn));
				
				mac_address = URLDecoder.decode(json.get("mac_address").toString());
				creation_time = URLDecoder.decode(json.get("creation_time").toString(), "UTF-8");
				last_updated = URLDecoder.decode(json.get("last_updated").toString());
				list_of_files = URLDecoder.decode(json.get("list_of_files").toString(), "UTF-8");
				user = URLDecoder.decode(json.get("user").toString(), "UTF-8");
				
				if (value_conn == 1) {
					int delay = 1000;
					int period = 5000;
					timer = new Timer();
					
					timer.scheduleAtFixedRate(new TimerTask() {
						
						public void run() {
							value_conn_counter = value_conn_counter + 1;
							if (value_conn_counter == value_conn) {
								System.out.println("Online");
								status = "Online";
								
								//status = status_of_device;

								StatusDeviceDAO status_of_device_dao = new StatusDeviceDAO();
								String return_date_of_creation = "-1";

								try {
									return_date_of_creation = status_of_device_dao.getStatusOfDeviceByMacAddress(mac_address);
									System.out.println("return_date_of_creation = " + return_date_of_creation);
								} catch (Exception e) {
									System.err.println(
											"Got an exception get file by mac_address: " + e.getMessage() + " - " + e.toString());
								}

								if (return_date_of_creation.equals("-1")) {
									StatusDeviceDAO.saveProjectProperty(mac_address, creation_time, last_updated, list_of_files, user,
											status);
									try {
										status_of_device_dao.insertStatusOfDevice(mac_address, creation_time, last_updated, list_of_files,
												user, status);
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									StatusDeviceDAO.saveProjectProperty(mac_address, return_date_of_creation, last_updated,
											list_of_files, user, status);
									try {
										status_of_device_dao.updateStatusOfDevice(mac_address, return_date_of_creation, last_updated,
												list_of_files, user, status);
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

								System.out.println("mac_address = " + mac_address);
								System.out.println("creation_time = " + creation_time);
								System.out.println("last_updated = " + last_updated);
								System.out.println("list_of_files = " + list_of_files);
								System.out.println("user = " + user);
								System.out.println("status = " + status);

								Properties prop = new Properties();
								InputStream input = null;

								try {

									input = new FileInputStream("config.properties");

									// load a properties file
									prop.load(input);

									// get the property value and print it out
									prop.setProperty("mac_address", mac_address);
									prop.setProperty("creation_time", creation_time);
									prop.setProperty("last_updated", last_updated);
									prop.setProperty("list_of_files", list_of_files);
									prop.setProperty("user", user);
									prop.setProperty("status", status);
									
									//response.sendRedirect("device.jsp");

									/*request.getSession().setAttribute("mac_address", mac_address);
									request.getSession().setAttribute("creation_time", creation_time);
									request.getSession().setAttribute("last_updated", last_updated);
									request.getSession().setAttribute("list_of_files", list_of_files);
									request.getSession().setAttribute("user", user);
									request.getSession().setAttribute("status", status);

									request.getRequestDispatcher("device.jsp").forward(request, response);*/

								} catch (IOException ex) {
									ex.printStackTrace();
								} finally {
									if (input != null) {
										try {
											input.close();
										} catch (IOException e) {
											e.printStackTrace();
										}
									}
								}
							} else if (value_conn_counter > value_conn) {
								System.out.println("Offline");
								status = "Offline";
								
								//status = status_of_device;

								StatusDeviceDAO status_of_device_dao = new StatusDeviceDAO();
								String return_date_of_creation = "-1";

								try {
									return_date_of_creation = status_of_device_dao.getStatusOfDeviceByMacAddress(mac_address);
									System.out.println("return_date_of_creation = " + return_date_of_creation);
								} catch (Exception e) {
									System.err.println(
											"Got an exception get file by mac_address: " + e.getMessage() + " - " + e.toString());
								}

								if (return_date_of_creation.equals("-1")) {
									StatusDeviceDAO.saveProjectProperty(mac_address, creation_time, last_updated, list_of_files, user,
											status);
									try {
										status_of_device_dao.insertStatusOfDevice(mac_address, creation_time, last_updated, list_of_files,
												user, status);
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									StatusDeviceDAO.saveProjectProperty(mac_address, return_date_of_creation, last_updated,
											list_of_files, user, status);
									try {
										status_of_device_dao.updateStatusOfDevice(mac_address, return_date_of_creation, last_updated,
												list_of_files, user, status);
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

								System.out.println("mac_address = " + mac_address);
								System.out.println("creation_time = " + creation_time);
								System.out.println("last_updated = " + last_updated);
								System.out.println("list_of_files = " + list_of_files);
								System.out.println("user = " + user);
								System.out.println("status = " + status);

								Properties prop = new Properties();
								InputStream input = null;

								try {

									input = new FileInputStream("config.properties");

									// load a properties file
									prop.load(input);

									// get the property value and print it out
									prop.setProperty("status", status);
									
									//response.sendRedirect("device.jsp");

									/*request.getSession().setAttribute("mac_address", mac_address);
									request.getSession().setAttribute("creation_time", creation_time);
									request.getSession().setAttribute("last_updated", last_updated);
									request.getSession().setAttribute("list_of_files", list_of_files);
									request.getSession().setAttribute("user", user);
									request.getSession().setAttribute("status", status);

									request.getRequestDispatcher("device.jsp").forward(request, response);*/

								} catch (IOException ex) {
									ex.printStackTrace();
								}							
								finally {
									if (input != null) {
										try {
											input.close();
										} catch (IOException e) {
											e.printStackTrace();
										}
									}
									value_conn_counter=0;
									value_conn=0;
									counter_connection=0;
									timer.cancel();
								}
								}
							}
						}
					, delay, period);
				} else if(value_conn == 3) {
					int delay = 1000;
					int period = 5000;
					timer.cancel();
					timer1 = new Timer();
					
					timer1.scheduleAtFixedRate(new TimerTask() {
						
						public void run() {
							value_conn_counter = value_conn_counter + 1;
							if (value_conn_counter == value_conn) {
								System.out.println("Online");
								status = "Online";
								
								//status = status_of_device;

								StatusDeviceDAO status_of_device_dao = new StatusDeviceDAO();
								String return_date_of_creation = "-1";

								try {
									return_date_of_creation = status_of_device_dao.getStatusOfDeviceByMacAddress(mac_address);
									System.out.println("return_date_of_creation = " + return_date_of_creation);
								} catch (Exception e) {
									System.err.println(
											"Got an exception get file by mac_address: " + e.getMessage() + " - " + e.toString());
								}

								if (return_date_of_creation.equals("-1")) {
									StatusDeviceDAO.saveProjectProperty(mac_address, creation_time, last_updated, list_of_files, user,
											status);
									try {
										status_of_device_dao.insertStatusOfDevice(mac_address, creation_time, last_updated, list_of_files,
												user, status);
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									StatusDeviceDAO.saveProjectProperty(mac_address, return_date_of_creation, last_updated,
											list_of_files, user, status);
									try {
										status_of_device_dao.updateStatusOfDevice(mac_address, return_date_of_creation, last_updated,
												list_of_files, user, status);
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

								System.out.println("mac_address = " + mac_address);
								System.out.println("creation_time = " + creation_time);
								System.out.println("last_updated = " + last_updated);
								System.out.println("list_of_files = " + list_of_files);
								System.out.println("user = " + user);
								System.out.println("status = " + status);

								Properties prop = new Properties();
								InputStream input = null;

								try {

									input = new FileInputStream("config.properties");

									// load a properties file
									prop.load(input);

									// get the property value and print it out
									prop.setProperty("mac_address", mac_address);
									prop.setProperty("creation_time", creation_time);
									prop.setProperty("last_updated", last_updated);
									prop.setProperty("list_of_files", list_of_files);
									prop.setProperty("user", user);
									prop.setProperty("status", status);
									
									//response.sendRedirect("device.jsp");

									/*request.getSession().setAttribute("mac_address", mac_address);
									request.getSession().setAttribute("creation_time", creation_time);
									request.getSession().setAttribute("last_updated", last_updated);
									request.getSession().setAttribute("list_of_files", list_of_files);
									request.getSession().setAttribute("user", user);
									request.getSession().setAttribute("status", status);

									request.getRequestDispatcher("device.jsp").forward(request, response);*/

								} catch (IOException ex) {
									ex.printStackTrace();
								} finally {
									if (input != null) {
										try {
											input.close();
										} catch (IOException e) {
											e.printStackTrace();
										}
									}
								}
							} else if (value_conn_counter > value_conn) {
								System.out.println("Offline");
								status = "Offline";
								
								//status = status_of_device;

								StatusDeviceDAO status_of_device_dao = new StatusDeviceDAO();
								String return_date_of_creation = "-1";

								try {
									return_date_of_creation = status_of_device_dao.getStatusOfDeviceByMacAddress(mac_address);
									System.out.println("return_date_of_creation = " + return_date_of_creation);
								} catch (Exception e) {
									System.err.println(
											"Got an exception get file by mac_address: " + e.getMessage() + " - " + e.toString());
								}

								if (return_date_of_creation.equals("-1")) {
									StatusDeviceDAO.saveProjectProperty(mac_address, creation_time, last_updated, list_of_files, user,
											status);
									try {
										status_of_device_dao.insertStatusOfDevice(mac_address, creation_time, last_updated, list_of_files,
												user, status);
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									StatusDeviceDAO.saveProjectProperty(mac_address, return_date_of_creation, last_updated,
											list_of_files, user, status);
									try {
										status_of_device_dao.updateStatusOfDevice(mac_address, return_date_of_creation, last_updated,
												list_of_files, user, status);
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

								System.out.println("mac_address = " + mac_address);
								System.out.println("creation_time = " + creation_time);
								System.out.println("last_updated = " + last_updated);
								System.out.println("list_of_files = " + list_of_files);
								System.out.println("user = " + user);
								System.out.println("status = " + status);

								Properties prop = new Properties();
								InputStream input = null;

								try {

									input = new FileInputStream("config.properties");

									// load a properties file
									prop.load(input);

									// get the property value and print it out
									prop.setProperty("status", status);
									
									//response.sendRedirect("device.jsp");

									/*request.getSession().setAttribute("mac_address", mac_address);
									request.getSession().setAttribute("creation_time", creation_time);
									request.getSession().setAttribute("last_updated", last_updated);
									request.getSession().setAttribute("list_of_files", list_of_files);
									request.getSession().setAttribute("user", user);
									request.getSession().setAttribute("status", status);

									request.getRequestDispatcher("device.jsp").forward(request, response);*/

								} catch (IOException ex) {
									ex.printStackTrace();
								}							
								finally {
									if (input != null) {
										try {
											input.close();
										} catch (IOException e) {
											e.printStackTrace();
										}
									}
									value_conn_counter=0;
									value_conn=0;
									counter_connection=0;
									timer1.cancel();
								}
								}
							}
						}
					, delay, period);
				}
				break;
			default:
				System.out.println("Unknown operation = " + oper);
				// throw new DeserializationException("oper = "+ oper);
			}
		} catch (Exception e) {
			System.err.println("Got an exception insertDatabase: " + e.getMessage() + " - " + e.toString());
		} finally {
		}
	}
		
		public static Timestamp getLogDate(String date) {		
	        try {
	            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	            Date currentTimeStamp = dateFormat.parse(date); // Find todays date   
	            Timestamp timestamp = new java.sql.Timestamp(currentTimeStamp.getTime());
	            
	            System.out.println("Current date: " + timestamp);
	            return timestamp;
	        } catch (Exception e) {
	            e.printStackTrace();
	            return (Timestamp) null;
	        }
	    }
}
