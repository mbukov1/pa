package pa;

import org.apache.log4j.Logger;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.json.simple.JSONArray;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.annotations.SerializedName;

import dao.DeviceLogDAO;
import model.DeviceLog;
import model.DeviceLogFromJson;
import model.Files;
import util.JSONUtil;
import util.QueryUtil;

import java.lang.reflect.Type;

public class PAServletJsonArray extends HttpServlet {
	
	static final Logger logger = Logger.getLogger(PAServletJsonArray.class);
	public static String MariaDB_URL = GetPropertyValues.getMariaDBPath();

	private static final long serialVersionUID = 1L;
	private static final String CONTENT_TYPE_HTML = "text/html; charset=utf8";
	private static final String CONTENT_TYPE_JSON = "application/json; charset=utf8";
	private static final String CHARACTER_ENCODING = "utf-8";
	
	public String oper = "";
	private Connection connect = null;
	
	@SerializedName("data")
	public DeviceLog device_log = new DeviceLog();
	@SerializedName("data")
	public ArrayList<DeviceLogFromJson> device_log_Array_List = null;
	
	public Gson gson = null;

	// @SuppressWarnings("unlikely-arg-type")
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("DO GET FROM SERVLET PAServlet!");
		PrintWriter out = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "UTF8"), true);
		response.setContentType(CONTENT_TYPE_JSON);
		response.setCharacterEncoding(CHARACTER_ENCODING);

		String url = request.getQueryString();
		String json = "";
		String mac = "";
		this.connect = GetPropertyValues.getLocalConnection();

		if (url != null) {
			System.out.println("url = " + url);
			Map<String, String> responseMap = QueryUtil.splitQuery(url);
			for (Map.Entry<String, String> entry : responseMap.entrySet()) {
				if ("oper".equals(((String) entry.getKey()).toLowerCase())) {
					oper = (String) entry.getValue().toLowerCase();
					System.out.println("oper = " + oper);
				} else if ("json".equals(((String) entry.getKey()).toLowerCase())) {
					json = (String) entry.getValue().toLowerCase();
					System.out.println("json = " + json);
				} else if ("mac".equals(((String) entry.getKey()).toLowerCase())) {
					mac = (String) entry.getValue().toLowerCase();
					System.out.println("mac = " + mac);
				} else {
					System.out.println("Unknown parameter: " + (String) entry.getValue());
				}
			}
			
			if ("testservice".equals(oper)) {
				response.setContentType(CONTENT_TYPE_HTML);
				out.print("The servlet has received a GET. This is the reply: TEST 1");
				out.close();
			}
			
			ArrayList<Files> jo = new ArrayList();
			JSONArray ja = new JSONArray();

			/*
			 * if ("video_data".equals(oper.toLowerCase())) {
			 * out.print(JSONUtil.getFilesJSONList()); }
			 */
//			if ("scheduler_files".equals(oper)) {
//				System.out.println("scheduler_files()");
//				ObjectOutputStream writer = new ObjectOutputStream(response.getOutputStream());
//
//				ArrayList<SchedulerFiles> arraylist = JSONUtil.getScheduler_files(mac);
//
//				writer.writeObject(arraylist);
//				writer.flush();
//				writer.close();
//
//				// out.print(JSONUtil.getScheduler_files());
//			} else if ("file_list".equals(oper)) {
//				ArrayList<SchedulerFiles> arraylist = JSONUtil.getScheduler_files(mac);
//				// ArrayList<Scheduler_files> arraylisttodayFiles =
//				// JSONUtil.todaysFiles(arraylist);
//
//				out.print(arraylist);
//			} else 
				if ("click".equals(oper)) {
				// jo = JSONUtil.getAlarms(p1);
				out.print(jo.toString());
			} else if ("uploadlog".equals(oper)) {
				// jo = JSONUtil.getAlarms(p1);
				out.print(jo.toString());
			} else if ("getlog".equals(oper)) {
				// jo = JSONUtil.getAlarms(p1);
				out.print(jo.toString());
			}

			if (out != null) {
				out.close();
			}
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
				String requestString = "";
				
				String line = "";
				while ((line = br.readLine()) != null) {
					requestString += line;
				}
				request.getInputStream().close();

				String recievedString = new String(requestString);
				System.out.println("recievedString = " + recievedString);
				
				String url = request.getQueryString();
				String json1 = "";
				if (url != null) {
					System.out.println("url = " + url);
					Map<String, String> responseMap = QueryUtil.splitQuery(url);
					for (Map.Entry<String, String> entry : responseMap.entrySet()) {
						if ("oper".equals(((String) entry.getKey()).toLowerCase())) {
							oper = (String) entry.getValue();
						} else {
							System.out.println("Unknown parameter: " + (String) entry.getValue());
						}
					}
				}
				
				System.out.println("oper = " + oper);
				
				JSONObject jo = new JSONObject();
				JSONArray ja = new JSONArray();
				if ("get_log".equals(oper.toLowerCase())) {
					device_log_Array_List = new ArrayList();
					
					String json = "";

                    json = recievedString.replace("[", "");
                    json = json.replace("]", "");
					
					Type type = new TypeToken<List<DeviceLogFromJson>>() {
                    }.getType();
                    gson = new Gson();
                    device_log_Array_List = gson.fromJson(recievedString, type);
					
					System.out.println("recievedString = " + recievedString);
					
					for(int i=0; i<device_log_Array_List.size(); i++) { 
					
					//JSONObject json = new JSONObject(recievedString);

					Long id = device_log_Array_List.get(i).getId();
					String MAC_address = device_log_Array_List.get(i).getMacAddress();
					String file_name = device_log_Array_List.get(i).getFileName();
					String play_start = device_log_Array_List.get(i).getPlayStart();
					String play_end = device_log_Array_List.get(i).getPlayEnd();
					String screen_size = device_log_Array_List.get(i).getScreenSize();
					String battery = device_log_Array_List.get(i).getBattery();
					String connection_number = device_log_Array_List.get(i).getConnectionNumber();
					String charging = device_log_Array_List.get(i).getCharging();
					String latitude = device_log_Array_List.get(i).getLatitude();
					String longitude = device_log_Array_List.get(i).getLongitude();
					String created = device_log_Array_List.get(i).getCreated();
					String updated = device_log_Array_List.get(i).getUpdated();
					String description = device_log_Array_List.get(i).getDescription();
					
					//String transferred = URLDecoder.decode(json.get("transferred").toString(), "UTF-8");
			
					System.out.println("json id: "+id);
					System.out.println("json MAC_address: "+MAC_address);
					System.out.println("json battery: "+battery);
					System.out.println("json charging: "+charging);
					System.out.println("json connection_number: "+connection_number);
					System.out.println("json file_name: "+file_name);
					System.out.println("json latitude: "+latitude);
					System.out.println("json longitude: "+longitude);
					System.out.println("json play_end: "+play_end);
					System.out.println("json play_start: "+play_start);
					System.out.println("json screen: "+screen_size);
					System.out.println("json created: "+created);
					System.out.println("json updated: "+updated);
					System.out.println("json description: "+description);
					//System.out.println("json transferred: "+transferred);
					
					//device_log.setId(id);
					device_log.setMacAddress(MAC_address);
					device_log.setFileName(file_name);
					device_log.setPlayStart(getLogDate(play_start));
					device_log.setPlayEnd(getLogDate(play_end));
					device_log.setScreenSize(screen_size);
					device_log.setBattery(Double.valueOf(battery));
					device_log.setConnectionNumber(Integer.parseInt(connection_number));
					device_log.setCharging(charging);
					device_log.setLatitude(Double.valueOf(latitude));
					device_log.setLongitude(Double.valueOf(longitude));
					device_log.setCreated(getLogDate(created));
					device_log.setUpdated(getLogDate(updated));
					device_log.setDescription(description);
					
					//System.out.println("json objects: "+json);
					
					DeviceLogDAO deviceLogDAO = new DeviceLogDAO();
					deviceLogDAO.insertDeviceLog(device_log);
					//device_log=null;
				
					}
				}
		} catch (Exception e) {
			System.err.println("Got an exception: " + e.getMessage() + " - " + e.toString());
		} finally {
		}
	}
	
	public static Timestamp getLogDate(String date) {		
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date currentTimeStamp = dateFormat.parse(date); // Find todays date   
            Timestamp timestamp = new java.sql.Timestamp(currentTimeStamp.getTime());
            
            System.out.println("Current date: " + timestamp);
            return timestamp;
        } catch (Exception e) {
            e.printStackTrace();
            return (Timestamp) null;
        }
    }
	/*
	 * public static void main(String[] args) {
	 * //System.out.println(JSONUtil.getServerJSONList(con));
	 * System.out.println(JSONUtil.getFilesJSONList()); }
	 */
}
