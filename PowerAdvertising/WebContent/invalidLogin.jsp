<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<script type="text/javascript">
	function openPage(pageURL) {
		window.location.href = pageURL;
	}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1256">
<title>Invalid Login</title>
</head>
<body>
	<center>Sorry, you are not a registered user! Please sign up
		first</center>
	<br>
	<center>
		<td>
			<button type="button" onclick="openPage('homepage.jsp')" class="back">Go
				Back</button>
		</td>
	</center>
</body>
</html>