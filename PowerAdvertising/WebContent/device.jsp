<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*" %> 
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Properties" %>
<%@ page import="pa.PAServletJsonArray" %>   
<%@ page import = "java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Device properties</title>
</head>
<%
response.setIntHeader("Refresh", 5);

Properties prop = new Properties();
InputStream input = null;

String mac_address = "";
String creation_time = "";
String last_updated = "";
String list_of_files = "";
String user = "";
String status = "";

try {

	input = new FileInputStream("config.properties");

	// load a properties file
	prop.load(input);

	// get the property value and print it out
	mac_address = prop.getProperty("mac_address");
	creation_time = prop.getProperty("creation_time");
	last_updated = prop.getProperty("last_updated");
	list_of_files = prop.getProperty("list_of_files");
	user = prop.getProperty("user");
	status = prop.getProperty("status");

} catch (IOException ex) {
	ex.printStackTrace();
} finally {
	if (input != null) {
		try {
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

List<String> array_list_of_files = null;

    try {
    array_list_of_files = new ArrayList<String>(Arrays.asList(list_of_files.split(", ")));
    }
    catch(Exception e) {
    	array_list_of_files = null;	
    }
%>
<body>
<center> 
      <font color="gray" size="4">  MAC address: <%= mac_address %>  </font> 
    </center>
    <br>
    <center> 
      <font color="black" size="3">  Creation Time: <%= creation_time %>  </font> 
    </center>
    <br>
    <center> 
      <font color="black" size="3">  Updated Time: <%= last_updated %>  </font> 
    </center>
    <br>
    <center> 
      <font color="gray" size="2">  Status: <%= status %>  </font> 
    </center>
    <br>
<table align="center" cellpadding="1" cellspacing="1" border="1">
<tr>
<tr bgcolor="#f0f8ff">
<td><b>List of files on device</b></td>
</tr>
<tr>
<tr bgcolor="#DEB887">
<% if(array_list_of_files != null) {
	for(int i=0; i<array_list_of_files.size(); i++) { %>
	<tr><td><%= array_list_of_files.get(i) %></td></tr> 
	<%} 
	}%>
</tr>
</table>
<br>
<center>
<td> <button type="button" onclick="openPage('index.jsp')" class="back">Go Back</button></td>
</center>
</body>
</html>
<script type="text/javascript">
 function openPage(pageURL)
 {
 window.location.href = pageURL;
 }
</script>